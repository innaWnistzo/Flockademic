import { createHash, randomBytes } from 'crypto';
import { v4 as uuid } from 'uuid';

import {
  PostNewSessionResponse,
} from '../../../../lib/interfaces/endpoints/accounts';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { signJwt } from '../../../../lib/lambda/signJwt';
import { insertSessionUuid } from '../dao';

export async function createNewSession(
  context: Request<{}> & DbContext,
): Promise<PostNewSessionResponse> {
  const generatedUuid = uuid();
  const hash = createHash('sha256').update(generatedUuid + Date.now().toString() + randomBytes(40).toString('hex'));
  const refreshToken = hash.digest('hex');

  try {
    await insertSessionUuid(context.database, generatedUuid, refreshToken);
  } catch (e) {
    throw new Error('Could not save generated UUID');
  }
  const jwt = signJwt({ identifier: generatedUuid });

  if (jwt instanceof Error) {
    throw new Error('We are currently experiencing issues, please try again later.');
  }

  return { session: { identifier: generatedUuid }, jwt, refreshToken };
}
