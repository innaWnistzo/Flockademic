import { abortOrcidVerification, initiateOrcidVerification, reducer } from '../../src/ducks/account';

jest.mock('../../src/services/account', () => ({
  getAccount: jest.fn().mockReturnValue(Promise.resolve(null)),
  getSession: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary account id' })),
  getToken: jest.fn().mockReturnValue(Promise.resolve('arbitrary_jwt')),
  verifyOrcid: jest.fn().mockReturnValue(Promise.resolve('arbitrary orcid')),
}));

describe('The account reducer', () => {
  it('should have verification in progress after initiating verification', () => {
    const newState = reducer(undefined, { type: 'flockademic/orcid/initiateVerification' });

    expect(newState.verifying).toBe(true);
  });

  it('should not have verification in progress after aborting verifications', () => {
    const newState = reducer(undefined, { type: 'flockademic/orcid/abortVerification' });

    expect(newState.verifying).toBe(false);
  });

  it('should not have verification in progress after failed verifications', () => {
    const newState = reducer(undefined, {
      type: 'flockademic/orcid/failVerification',
    });

    expect(newState.verifying).toBe(false);
  });

  it('should not have verification in progress after failed hydrations', () => {
    const newState = reducer(undefined, { type: 'flockademic/orcid/failHydration' });

    expect(newState.verifying).toBe(false);
  });

  it('should store the ORCID on successful verifications', () => {
    const newState = reducer(undefined, { type: 'flockademic/orcid/verify', payload: { orcid: 'Some ORCID' } });

    expect(newState.orcid).toBe('Some ORCID');
    expect(newState.verifying).toBe(false);
  });

  it('should return the previous state for invalid actions', () => {
    const mockState = { verifying: true };
    expect(reducer(mockState, { type: 'arbitrary invalid action' })).toBe(mockState);
  });
});

describe('The ORCID verification action creator', () => {
  it('should dispatch an initiation action as soon as it starts the verification process', () => {
    const thunk = initiateOrcidVerification();
    const mockDispatch = jest.fn();

    thunk(mockDispatch);

    expect(mockDispatch.mock.calls.length).toBe(1);
    expect(mockDispatch.mock.calls[0][0]).toMatchSnapshot();
  });

  it('should dispatch a verification action if the ORCID could be retrieved from local storage', (done) => {
    const thunk = initiateOrcidVerification();
    const mockDispatch = jest.fn();

    const mockGetOrcid = require.requireMock('../../src/services/account').getAccount;
    mockGetOrcid.mockReturnValueOnce(Promise.resolve({ identifier: 'Some account ID', orcid: 'Some ORCID' }));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0].payload.accountId).toBe('Some account ID');
      expect(mockDispatch.mock.calls[1][0].payload.orcid).toBe('Some ORCID');
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });

  // tslint:disable-next-line:max-line-length
  it('should dispatch a "fail hydration" action if the ORCID could not be retrieved from local storage and no verification code was provided', (done) => {
    const thunk = initiateOrcidVerification();
    const mockDispatch = jest.fn();

    const mockGetOrcid = require.requireMock('../../src/services/account').getAccount;
    mockGetOrcid.mockReturnValueOnce(Promise.resolve(null));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });

  // tslint:disable-next-line:max-line-length
  it('should not dispatch a "fail hydration" action if the ORCID could not be retrieved from local storage but a verification code was provided', (done) => {
    const thunk = initiateOrcidVerification('Arbitrary redirect URI', 'Arbitrary verification code');
    const mockDispatch = jest.fn();

    const mockGetOrcid = require.requireMock('../../src/services/account').getAccount;
    mockGetOrcid.mockReturnValueOnce(Promise.resolve(null));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls[1][0].type).not.toBe('flockademic/orcid/failHydration');
      done();
    });
  });

  it('should dispatch a verification failure action if local storage could not be accessed', (done) => {
    const thunk = initiateOrcidVerification();
    const mockDispatch = jest.fn();

    const mockGetOrcid = require.requireMock('../../src/services/account').getAccount;
    mockGetOrcid.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });

  it('should dispatch a verification failure action if the user\'s account could not be fetched', (done) => {
    const thunk = initiateOrcidVerification('Arbitrary redirect URL', 'Arbitrary verification code');
    const mockDispatch = jest.fn();

    const mockGetAccount = require.requireMock('../../src/services/account').getSession;
    mockGetAccount.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });

  it('should dispatch a verification failure action if no JSON Web Token could be retrieved', (done) => {
    const thunk = initiateOrcidVerification('Arbitrary redirect URL', 'Arbitrary verification code');
    const mockDispatch = jest.fn();

    const mockGetToken = require.requireMock('../../src/services/account').getToken;
    mockGetToken.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });

  it('should dispatch a verification failure action if the verification call failed', (done) => {
    const thunk = initiateOrcidVerification('Arbitrary redirect URL', 'Arbitrary verification code');
    const mockDispatch = jest.fn();

    const mockVerifyOrcid = require.requireMock('../../src/services/account').verifyOrcid;
    mockVerifyOrcid.mockReturnValueOnce(Promise.reject(new Error('Arbitrary error')));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });

  it('should dispatch a verification action on successful verification', (done) => {
    const thunk = initiateOrcidVerification('Arbitrary redirect URL', 'Arbitrary verification code');
    const mockDispatch = jest.fn();

    const mockVerifyOrcid = require.requireMock('../../src/services/account').verifyOrcid;
    mockVerifyOrcid.mockReturnValueOnce(Promise.resolve({ identifier: 'Some account ID', orcid: 'Some ORCID' }));

    thunk(mockDispatch);

    setImmediate(() => {
      expect(mockDispatch.mock.calls.length).toBe(2);
      expect(mockDispatch.mock.calls[1][0].payload.accountId).toBe('Some account ID');
      expect(mockDispatch.mock.calls[1][0].payload.orcid).toBe('Some ORCID');
      expect(mockDispatch.mock.calls[1][0]).toMatchSnapshot();
      done();
    });
  });
});

describe('The verification abortion action creator', () => {
  it('should dispatch an abort action', () => {
    expect(abortOrcidVerification()).toMatchSnapshot();
  });
});
