const orcidUrlRegex = /https?:\/\/orcid.org\/\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)\/?/;
const orcidRegex = /\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X)/;

export function getOrcid(
  websites: string | { sameAs: string; roleName?: string; } | Array<string | { sameAs: string; roleName?: string; }>,
): string | null {
  if (!Array.isArray(websites)) {
    websites = [ websites ];
  }
  const urls = websites.map((website) => (typeof website === 'string') ? website : website.sameAs);

  const orcids = urls.filter((url: string) => orcidUrlRegex.test(url));

  if (orcids.length === 0) {
    return null;
  }

  const matches = orcids[0].match(orcidRegex);

  /* istanbul ignore if: Impossible to reach unless `orcidRegex` diverges from `orcidUrlRegex` */
  if (matches === null) {
    return null;
  }

  return matches[0];
}
